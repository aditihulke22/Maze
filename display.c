#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ncurses.h>
#define SIZE 15
int main(int argc, char *argv[]){
  initscr();
  start_color();
  keypad(stdscr, TRUE);
  curs_set(0);
  WINDOW *win = newwin(10, 100, 20, 20);
  int fd, i = 1, j = 1, k = 0, l = 0, m = 0, ch;
  char c, a[SIZE][SIZE];
  init_pair(1, 1, 2);
  init_pair(2, 3, 7);
  fd = open(argv[1], O_RDONLY, S_IRUSR | S_IWUSR);
  while(read(fd, &c, sizeof(char))){
    if(c == '\n'){
      k++;
      l = m;
      m = 0;
    }
    else{
      a[k][m] = c;
      m++;
    }
  }
  attrset(COLOR_PAIR(1));
  for(i = 0; i < k; i++){
    for(j = 0; j < k; j++){
      c = a[i][j];
      if(c == 'r'){
	attrset(COLOR_PAIR(1));
	printw(" ");
      }
      else if(c == 'h'){
	attrset(COLOR_PAIR(1));
	printw(" ");
      }
      else if(c == 'S'){
	attrset(COLOR_PAIR(1));
	printw("S");
      }
      else if(c == 'v'){
	attrset(COLOR_PAIR(1));
	printw(" ");
      }
      else if(c == '0'){
	attrset(COLOR_PAIR(3));
	printw(" ");
      }
      else if(c == 'a'){
	attrset(COLOR_PAIR(3));
	printw(" ");
      }
      else if(c == 'p' || c == 'f'){
	attrset(COLOR_PAIR(3));
	printw(" ");
      }
      else if(c == 'E'){
	attrset(COLOR_PAIR(1));
	printw("E");
      }
    }
    printw("\n");
  }
  move(SIZE + 1, 0);
  attrset(COLOR_PAIR(2));
  printw("Press UP Arrow key to go UP\nPress DOWN Arrow key to go DOWN\nPress LEFT Arrow key to go RIGHT\nPress RIGHT Arrow key to go RIGHT");
  for(i = 1; i < k; i++){
    for(j = 1; j < l; j++){
      if(a[i][j] == 'S'){
	break;
      }
    }
    break;
  }
  move(i, j);
  while(ch != KEY_HOME){
    ch = getch();
    move(SIZE + 1, 0);
    attrset(COLOR_PAIR(2));
    printw("Press UP Arrow key to go UP\nPress DOWN Arrow key to go DOWN\nPress LEFT Arrow key to go RIGHT\nPress RIGHT Arrow key to go RIGHT\nPress HOME key to exit");
    switch(ch){
    case KEY_UP:
      if(a[i -1][j] == 'p' || a[i - 1][j] == '0' || a[i - 1][j] == 'E' || a[i - 1][j] == 'f'){
	i--;
	move(i, j);
	attrset(COLOR_PAIR(2));
	printw(" ");
      }
      else{
	move(SIZE + 1, 0);
	printw("Invalid move");
	attrset(COLOR_PAIR(3));
	for(k = 0; k < 5; k++){
	  for(l = 0; l < 33; l++){
	    printw(" ");
	  }
	  printw("\n");
	}
	move(i, j);
      }
      break;
    case KEY_DOWN:
      if(a[i + 1][j] == 'p' || a[i + 1][j] == '0' || a[i + 1][j] == 'E' || a[i + 1][j] == 'f'){
	i++;
	move(i, j);
	attrset(COLOR_PAIR(2));
	printw(" ");
      }
      else{
	move(SIZE + 1, 0);
	printw("Invalid move");
	attrset(COLOR_PAIR(3));
	for(k = 0; k < 5; k++){
	  for(l = 0; l < 33; l++){
	    printw(" ");
	  }
	  printw("\n");
	}
	move(i, j);
      }
      break;
    case KEY_LEFT:
      if(a[i][j - 1] == 'p' || a[i][j - 1] == '0' || a[i][j - 1] == 'E' || a[i][j - 1] == 'f'){
	j--;
	move(i, j);
	attrset(COLOR_PAIR(2));
	printw(" ");
      }
      else{
	move(SIZE + 1, 0);
	printw("Invalid move");
	attrset(COLOR_PAIR(3));
	for(k = 0; k < 5; k++){
	  for(l = 0; l < 33; l++){
	    printw(" ");
	  }
	  printw("\n");
	}
	move(i, j);
      }
      break;
    case KEY_RIGHT:
      if(a[i][j + 1]== 'p' || a[i][j + 1] == '0' || a[i][j + 1] == 'E' || a[i][j + 1] == 'f'){
	j++;
	move(i, j);
	attrset(COLOR_PAIR(2));
	printw(" ");
      }
      else{
	move(SIZE + 1, 0);
	printw("Invalid move");
	attrset(COLOR_PAIR(3));
	for(k = 0; k < 5; k++){
	  for(l = 0; l < 33; l++){
	    printw(" ");
	  }
	  printw("\n");
	}
	move(i, j);
      }
      break;
    case KEY_HOME:
      move(SIZE + 1, 0);
      printw("Press any key to return");
      attrset(COLOR_PAIR(3));
      for(k = 0; k < 5; k++){
	for(l = 0; l < 33; l++){
	  printw(" ");
	}
	printw("\n");
      }
      break;
    default:
      move(SIZE + 1, 0);
      printw("Invalid move");
      attrset(COLOR_PAIR(3));
	for(k = 0; k < 5; k++){
	  for(l = 0; l < 33; l++){
	    printw(" ");
	  }
	  printw("\n");
	}
      move(i, j);
      break;
    }
    if(a[i][j] == 'E'){
      move(SIZE, 0);
      printw("YOU WON. Press any key to exit");
      attrset(COLOR_PAIR(3));
      for(k = 0; k < 5; k++){
	for(l = 0; l < 33; l++){
	  printw(" ");
	}
	printw("\n");
      }
      move(i, j);
      break;
    }
  }
  close(fd);
  getch();
  endwin();
  return 0;
}
