#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <ncurses.h>
#define SIZE 15

int main(int argc, char *argv[]){
  int a[SIZE][SIZE];
  int *arr = (int*)malloc(sizeof(int) * (((SIZE - 1) / 2) * ((SIZE - 1) / 2) - 1));
  int i = 1, j = 1, k, l, h, count = 0, fd, detect = 1, t;
  char n = '\n';
  /*
   * 'r' is for 'restricted'
   * 'h' is for 'horizontal wall'
   * 'v' is for 'vertical wall'
   * 'a' is for 'allowed'
   * 'S' is for 'start'
   * 'E' is for 'end'
   * 'p' is for 'path'
   * '0' is for 'broken wall'
   */
  for(k = 0; k < SIZE; k = k + 2){
    a[0][k] = 'r';
    a[SIZE - 1][k] = 'r';
    a[k][0] = 'r';
    a[k][SIZE - 1] = 'r';
  }
  for(k = 1; k < SIZE; k = k + 2){
    a[0][k] = 'h';
    a[SIZE - 1][k] = 'h';
    a[k][0] = 'v';
    a[k][SIZE - 1] = 'v';
  }
  for(k = 1; k < SIZE - 1; k = k + 2){
    for(l = 2; l < SIZE - 1; l = l + 2){
      a[k][l] = 'v';
    }
    for(l = 1; l < SIZE - 1; l = l + 2){
      a[k][l] = 'a';
    }
  }
  for(k = 2; k < SIZE - 1; k = k + 2){
    for(l = 1; l < SIZE - 1; l = l + 2){
      a[k][l] = 'h';
    }
    for(l = 2; l < SIZE - 1; l = l + 2){
      a[k][l] = 'r';
    }
  }
  a[1][1] = 'S';
  //int arr[15] = {3, 1, 1, 4, 4, 2, 2, 1, 4, 2, 2, 1, 1, 2, 3};
  srand(time(0));
  for(k = 0; k < (((SIZE - 1) / 2) * ((SIZE - 1) / 2) - 1); k++){
    arr[k] = rand() % 4 + 1;
    //printf("%d ", arr[k]);
  }
  //printf("\n");
  for(h = 0; h < (((SIZE - 1) / 2) * ((SIZE - 1) / 2) - 1); h++){
    switch(arr[h]){
    case 1:
      
      while(1){
	count = 0;
	if(i != (SIZE - 2) && a[i + 2][j] == 'a'){
	  a[i + 2][j] = 'p';
	  a[i + 1][j] = '0';
	  detect = 1;
	  break;
	}
	else if(i != 1 && a[i - 2][j] == 'a'){
	  a[i - 2][j] = 'p';
	  a[i - 1][j] = '0';
	  detect = 2;
	  break;
	}
	else if(j != (SIZE - 2) && a[i][j + 2] == 'a'){
	  a[i][j + 2] = 'p';
	  a[i][j + 1] = '0';
	  detect = 3;
	  break;
	}
	else if(j != 1 && a[i][j - 2] == 'a'){
	  a[i][j - 2] = 'p';
	  a[i][j - 1] = '0';
	  detect = 4;
	  break;
	}
	else{
	  for(k = 1; k < SIZE - 1; k++){
	    for(l = 1; l < SIZE - 1; l++){
	      if(a[k][l] == 'a'){
		count++;
	      }
	    }
	  }
	  if(count > 0){
	    if(a[i + 1][j] == '0' && a[i + 2][j] == 'p'){
	      a[i][j] = 'f';
	      i= i + 2;
	      continue;
	    }
	    else if(a[i - 1][j] == '0' && a[i - 2][j] == 'p'){
	      a[i][j] = 'f';
	      i= i - 2;
	      continue;
	    }
	    else if(a[i][j + 1] == '0' && a[i][j + 2] == 'p'){
	      a[i][j] = 'f';
	      j = j + 2;
	      continue;
	    }
	    else if(a[i][j - 1] == '0' && a[i][j - 2] == 'p'){
	      a[i][j] = 'f';
	      j = j - 2;
	      continue;
	    }
	    else{
	    }
	  }
	  else{
	    break;
	  }
	}
      }
      
      if(detect == 1){
	i= i + 2;
      }
      else if(detect == 2){
	i= i - 2;
      }
      else if(detect == 3){
	j = j + 2;
      }
      else if(detect == 4){
	j = j - 2;
      }
      else{
      }
      break;
      
    case 2:

      while(1){
	count = 0;
	if(j != (SIZE - 2) && a[i][j + 2] == 'a'){
	  a[i][j + 2] = 'p';
	  a[i][j + 1] = '0';
	  detect = 3;
	  break;
	}
	else if(j != 1 && a[i][j - 2] == 'a'){
	  a[i][j - 2] = 'p';
	  a[i][j - 1] = '0';
	  detect = 4;
	  break;
	}
	else if(i != (SIZE - 2) && a[i + 2][j] == 'a'){
	  a[i + 2][j] = 'p';
	  a[i + 1][j] = '0';
	  detect = 1;
	  break;
	}
	else if(i != 1 && a[i - 2][j] == 'a'){
	  a[i - 2][j] = 'p';
	  a[i - 1][j] = '0';
	  detect = 2;
	  break;
	}
	else{
	  for(k = 1; k < SIZE - 1; k++){
	    for(l = 1; l < SIZE - 1; l++){
	      if(a[k][l] == 'a'){
		count++;
	      }
	    }
	  }
	  if(count > 0){
	    if(a[i][j + 1] == '0' && a[i][j + 2] == 'p'){
	      a[i][j] = 'f';
	      j = j + 2;
	      continue;
	    }
	    else if(a[i][j - 1] == '0' && a[i][j - 2] == 'p'){
	      a[i][j] = 'f';
	      j = j - 2;
	      continue;
	    }
	    else if(a[i + 1][j] == '0' && a[i + 2][j] == 'p'){
	      a[i][j] = 'f';
	      i= i + 2;
	      continue;
	    }
	    else if(a[i - 1][j] == '0' && a[i - 2][j] == 'p'){
	      a[i][j] = 'f';
	      i= i - 2;
	      continue;
	    }
	    else{
	    }
	  }
	  else{
	    break;
	  }
	}
      }
      
      if(detect == 1){
	i= i + 2;
      }
      else if(detect == 2){
	i= i - 2;
      }
      else if(detect == 3){
	j = j + 2;
      }
      else if(detect == 4){
	j = j - 2;
      }
      else{
      }
      break;
    case 3:

      while(1){
	count = 0;
	if(i != 1 && a[i - 2][j] == 'a'){
	  a[i - 2][j] = 'p';
	  a[i - 1][j] = '0';
	  detect = 2;
	  break;
	}
	else if(i != (SIZE - 2) && a[i + 2][j] == 'a'){
	  a[i + 2][j] = 'p';
	  a[i + 1][j] = '0';
	  detect = 1;
	  break;
	}
	else if(j != 1 && a[i][j - 2] == 'a'){
	  a[i][j - 2] = 'p';
	  a[i][j - 1] = '0';
	  detect = 4;
	  break;
	}
	else if(j != (SIZE - 2) && a[i][j + 2] == 'a'){
	  a[i][j + 2] = 'p';
	  a[i][j + 1] = '0';
	  detect = 3;
	  break;
	}
	else{
	  for(k = 1; k < SIZE - 1; k++){
	    for(l = 1; l < SIZE - 1; l++){
	      if(a[k][l] == 'a'){
		count++;
	      }
	    }
	  }
	  if(count > 0){
	    if(a[i - 1][j] == '0' && a[i - 2][j] == 'p'){
	      a[i][j] = 'f';
	      i = i - 2;
	      continue;
	    }
	    else if(a[i + 1][j] == '0' && a[i + 2][j] == 'p'){
	      a[i][j] = 'f';
	      i = i + 2;
	      continue;
	    }
	    else if(a[i][j - 1] == '0' && a[i][j - 2] == 'p'){
	      a[i][j] = 'f';
	      j = j - 2;
	      continue;
	    }
	    else if(a[i][j + 1] == '0' && a[i][j + 2] == 'p'){
	      a[i][j] = 'f';
	      j = j + 2;
	      continue;
	    }
	    else{
	    }
	  }
	  else{
	    break;
	  }
	}
      }
      
      if(detect == 1){
	i = i + 2;
      }
      else if(detect == 2){
	i = i - 2;
      }
      else if(detect == 3){
	j = j + 2;
      }
      else if(detect == 4){
	j = j - 2;
      }
      else{
      }
      break;
    case 4:

      while(1){
	count = 0;
	if(j != 1 && a[i][j - 2] == 'a'){
	  a[i][j - 2] = 'p';
	  a[i][j - 1] = '0';
	  detect = 4;
	  break;
	}
	else if(j != (SIZE - 2) && a[i][j + 2] == 'a'){
	  a[i][j + 2] = 'p';
	  a[i][j + 1] = '0';
	  detect = 3;
	  break;
	}
	else if(i != 1 && a[i - 2][j] == 'a'){
	  a[i - 2][j] = 'p';
	  a[i - 1][j] = '0';
	  detect = 2;
	  break;
	}
	else if(i != (SIZE - 2) && a[i + 2][j] == 'a'){
	  a[i + 2][j] = 'p';
	  a[i + 1][j] = '0';
	  detect = 1;
	  break;
	}
	else{
	  for(k = 1; k < SIZE - 1; k++){
	    for(l = 1; l < SIZE - 1; l++){
	      if(a[k][l] == 'a'){
		count++;
	      }
	    }
	  }
	  if(count > 0){
	    if(a[i][j - 1] == '0' && a[i][j - 2] == 'p'){
	      a[i][j] = 'f';
	      j = j - 2;
	      continue;
	    }
	    else if(a[i][j + 1] == '0' && a[i][j + 2] == 'p'){
	      a[i][j] = 'f';
	      j = j + 2;
	      continue;
	    }
	    else if(a[i - 1][j] == '0' && a[i - 1][j] == 'p'){
	      a[i][j] = 'f';
	      i = i - 2;
	      continue;
	    }
	    else if(a[i + 1][j] == '0' && a[i + 2][j] == 'p'){
	      a[i][j] = 'f';
	      i = i + 2;
	      continue;
	    }
	    else{
	    }
	  }
	  else{
	    break;
	  }
	}
      }
      
      if(detect == 1){
	i= i + 2;
      }
      else if(detect == 2){
	i= i - 2;
      }
      else if(detect == 3){
	j = j + 2;
      }
      else if(detect == 4){
	j = j - 2;
      }
      else{
      }
      break;
    }
  }

  /*srand(time(0));
  while(1){
    t = rand() % (SIZE) + 1;
    if(a[SIZE - 2][t] == 'f' || a[SIZE - 2][t] == 'p' || a[SIZE - 2][t] == '0'){
      a[SIZE - 2][t] = 'E';
      break;
    }
    }*/

  i = SIZE - 2;
  j = i;
  while(1){
    if(a[i][j] == 'f' || a[i][j] == 'p' || a[i][j] == '0'){
      a[i][j] = 'E';
      break;
    }
    else{
      j--;
    }
  }
  
  fd = open(argv[1], O_CREAT | O_RDONLY | O_WRONLY, S_IRUSR | S_IWUSR);
  for(k = 0; k < SIZE; k++){
    for(l = 0; l < SIZE; l++){
      write(fd, &a[k][l], sizeof(char));
    }
    write(fd, &n, sizeof(char));
  }
  close(fd);
  free(arr);
  return 0;
}
