Title - Maze Game
MIS - 111608028
Name - Aditi Hulke

     This is a maze game in which each maze is generated using the depth-first search algorithm and the generated maze is stored in a file. This work is done by maze.c.
     display.c reads the maze from that file and prints the equivalent maze in graphical form. The user has to go from S(start) to E(end). The game ends when he reaches E. To generate the maze again, user has to run maze.c and display.c again.
    Compile commands are-
    cc maze.c -o maze
    ./maze filename        /* say, level1*/
    cc display.c -o project
    ./project filename
    
    Some test files are given here- l1, l2, l3, etc.
    you can run them by ./project l1